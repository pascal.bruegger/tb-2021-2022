---
version: 3
type: tb
academic_year: 2021/2022
title: "Happy Baby : Application Mobile pour mise en relation parents, baby sitters et mamans de jour"
departments:
  - Informatique
orientations:
  - Informatique logicielle
max_students: 1
mandantors:
  - Cedric Mujynya
assigned_to : [Cedric Mujynya]  
professors: [Pascal Bruegger, Arton Hoxha]
instituts:
  - iCoSys
keywords: [Baby sittings, Application Mobile, Flutter]
languages: [F,E]
confidential: False
continuation: True

---
\begin{center}
\includegraphics[width=0.3\textwidth]{images/smartphone1.png}
\end{center}

## Description
l'idée est de développer et mettre sur le marché, via les stores Apple et Google, une application mobile permettant de mettre en contacts des parents cherchant une nounou pour s’occuper de leur enfant.  
Pour ce faire, l’application sera un mix entre Uber et Tinder :  
Le parent peut voir les mamans de jours/nounous proches de sa localisation (sans pour autant connaître la localisation précise de la personne, données privées): Similaire à Uber.  
Si le parent est interessé par le profil de la personne, il peut envoyer un "like" indiquant que le parent souhaite prendre contact. La nounou peut alors voir les informations de base de/des enfant(s) à garder et décider si oui ou non elle accepte la conversation: Similaire à Tinder

## Buts
Suite à un projet de semestre 6 et une étude de faisabilité, le but du projet est de réaliser une étude réel de marchée, concevoir et développer l'applications mobile partir du POC (Proof fo concept). Une réflexion sur le backend sera réaliser ainsi qu'une solution sur paiement des prestations.
Afin une mise en production par le dépôt sur les stores Apple et Google sera réaliser.

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse du POC et des fonctionlités supplémentaires à ajouter afin d'obtenir une application professionnelle. 
- Analyse approfondie sur la partie backend (privé, AWS, Firebase, princing, etc.)
- Conception de l'application mobile et du backend.
- Implementation de l'application mobile et du backend.
- Tests et validation du système.
- Déploiement sur les stores Apple et Google.
- Rédaction d'une documentation adéquate.
