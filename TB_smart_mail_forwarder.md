---
version: 3
title: Smart email forwarder
type: tb
academic_year: 2021/2022
departments:
  - Informatique
max_students: 1
mandantors: [Léonard Noth]
professors : [Pascal bruegger, Leonard Noth]
keywords: [email, forward, dynamic, redirection, interface, web]
languages: [F]
confidential: True
continuation: False
---


## Contexte

PYME.ch, une petite entreprise d'informatique, offre à ses clients la possibilité d'héberger leurs adresses email avec des noms de domaine personnalisé sur un service réalisé sur mesure. Ce service utilise Amazon SES (Simple Email Service) pour recevoir et envoyer des emails selon divers règles. Il permet également de rediriger des emails arrivant sur une boîte mail spécifique, et de réexpédier des emails à une liste de diffusion qu'il est possible de changer dynamiquement.

Cependant, toutes ces fonctionnalités sont implémentées à l'aide de code en Javascript, et pour modifier les règles de redirection, il est nécessaire de redéployer un script qui organise la manière dont sont gérées les emails entrants et/ou sortants. Afin de simplifier le processus de redirection de mail et de le rendre accessible à ses clients, PYME souhaite améliorer son système de redirection et de diffusion de mail.

Ce projet consiste à la création d'une interface dynamique permettant d'éditer des règles de redirection et des listes de diffusion sur des adresses email personnalisées, et de les appliquer de manière dynamique aux emails entrants.

## Contraintes du projet

- Le projet utilise Amazon SES pour envoyer et recevoir des emails, en se basant sur les scripts existants
- L'interface est développée à l'aide d'un framework web (React, Angular, VueJS, ...)

## Objectifs / Tâches

- Élaboration d'un cahier des charges répondant aux attentes du mandant
- Analyse des fonctionalités demandées
- Conception et implémentation d'une interface qui répond à la demande du mandant
- Tests et validation de l'application
- Documentation appropriées
