---
version: 3
type: tb
academic_year: 2021/2022
title: "Ecomu: Logiciel de gestion pour école de musique"
departments:
  - Informatique
orientations:
  - Informatique logicielle
max_students: 2
mandantors:
  - SoftDesisign SaRL
professors: [Pascal Bruegger, Arton Hoxha]
instituts:
  - iCoSys
keywords: [Logiciel de gestion, Ecole de musique, Application Mobile, Flutter]
languages: [F,E]
confidential: False
continuation: False

---
\begin{center}
\includegraphics[width=0.6\textwidth]{images/Ecomu.png}
\end{center}


## Description
l'idée de ce projet est de développer un logiciel de gestion pour une école de musique avec des technologies récentes. L'application finale comprendra: 
1. Un module de gestion des éléves, des incriptions et de la facturation, 
2. Une application mobile avec calendrier des cours et des studios pour les professeurs de musique,
3. Et un module de stastistique destiné à la direction.

## Buts
La particulararité de cette application est qu'elle pourra être developpée avec Flutter et ce pour Windows (nouveau module Flutter), Mac OSX, Web et Mobile afin de tester les nouvelles fonctinalité de Flutter. Mais l' (les) étudiant(s) sera (-ont) libre de choisir les technologies pour la réalistation du frontend mais aussi du bakend.
La charge et le nombre de modules developpés sera décidé en fonction du nombre d'étudiant.

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse des technologies frontend 
- Analyse approfondie sur la partie backend (NodeJS, MySQL, etc.)
- Conception de l'application du backend.
- Conception de l'application de Gestion.
- Conception de l'application mobile (smartphone et tablette) pour les professeurs
- Conception de l'application web pour les professeurs
- Implémentation du backend
- Implémentation de l'application de Gestion
- Implémentation de l'application mobile (smartphone et tablette) pour les professeurs
- Implémentation de l'application web pour les professeurs
- Tests et validation du système.
- Rédaction d'une documentation adéquate.

